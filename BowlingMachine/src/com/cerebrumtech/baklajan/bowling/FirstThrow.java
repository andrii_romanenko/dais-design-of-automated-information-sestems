package com.cerebrumtech.baklajan.bowling;

/**
 * User: andrey
 * Date: 3/1/14
 * Time: 2:50 PM
 */
public class FirstThrow extends AbstractThrow {

    public FirstThrow(boolean failed, Integer pinsDown) {
        super.setFailed(failed);
        super.setPinsDown(pinsDown);
    }
}
