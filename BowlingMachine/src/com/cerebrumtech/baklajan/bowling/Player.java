package com.cerebrumtech.baklajan.bowling;

/**
 * User: andrey
 * Date: 3/1/14
 * Time: 2:51 PM
 */
public interface Player {

    public Integer getScore();

    public void enterFrameScore(Integer firstThrow, Integer secondThrow, Integer additionalThrow);

    public String showScoreTable();

    public Integer getFramesPlayed();
}
