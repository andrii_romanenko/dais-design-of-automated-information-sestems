package com.cerebrumtech.baklajan.bowling;

import java.util.ArrayList;

/**
 * User: andrey
 * Date: 3/1/14
 * Time: 2:47 PM
 * <h1>Sorry for your time spent reading!</h1>
 * @author Andrii Romanenko
 * @since 23.03.2014
 */
public class BowlingMachine {
    /**
     * <p>This private variable indicates if it is possible to show results of the game</p>
     */
    private boolean gameStarted;
    private ArrayList<Person> players = new ArrayList<Person>();

    /**
     * <p>Adds a new Player to the game</p>
     * @param name The name of a new Player
     */
    public void addPlayer(String name) {
        if (players.size() < 4)
            players.add(new Person(name));
    }

    /**
     * <p>Changes specified players name</p>
     * @param id The id of the Player whose name should be changed
     * @param name The new name for Person
     */
    public void changePlayersName(Integer id, String name) {
        if (id < players.size())
            players.get(id).setName(name);
    }

    /**
     * <p>Deletes specified Player</p>
     * @param id The id of the Player who should be deleted
     */
    public void deletePlayer(Integer id) {
        if (id < players.size())
            players.remove((int) id);
    }

    /**
     * <p>Shows current Winner</p>
     * @return Winner(s) name(s) as a String
     */
    public String showCurrentWinner() {
        if (!gameStarted) return "";
        switch(players.size()) {
            case 0:
                return "";
            case 1:
                return players.get(0).getName();
            default:
                String result = "\nWinner:  ";
                String winner = players.get(0).getName();
                int maxScore = players.get(0).getScore();
                for (int i = 1; i < players.size(); i++) {
                    if (players.get(i).getScore() > maxScore) {
                        winner = players.get(i).getName();
                        maxScore = players.get(i).getScore();
                    } else if (players.get(i).getScore() == maxScore) {
                        winner += " & " + players.get(i).getName();
                    }
                }
                return result + winner + "\n";
        }
    }

    /**
     * <p>Shows result Table</p>
     * @return result Table as a String
     */
    public String showResultTable() {
        String result = "Name\t\t\t 1    2    3    4    5    6    7    8    9    10       Total";
        for (Person player : players) {
            result += "\n" + player.getName();

            result += (player.getName().length() > 8) ? "\t\t" : "\t\t\t";

            if (gameStarted) {
                result += player.showScoreTable();
            }
        }
        return result;
    }

    /**
     * <p>Indicates the next Player</p>
     * @return next players Id
     */
    public Integer getNextPlayer() {
        switch(players.size()) {
            case 0:
                return null;
            case 1:
                return 0;
            default:
                if (!gameStarted) return 0;
                Integer result = 0;
                Integer minFrames = players.get(0).getFramesPlayed();
                for (int i = 1; i < players.size(); i++) {
                    if (players.get(i).getFramesPlayed() < minFrames) {
                        result = i;
                        minFrames = players.get(i).getFramesPlayed();
                    }
                }
                return result;
        }
    }

    /**
     * <p>Shows specified players points</p>
     * @param index specifies player id
     * @return Players points or null if index is out of bounds
     */
    public Integer getPlayersPoints(int index) {
        if (index >= players.size()) return null;
        return players.get(index).getScore();
    }

    /**
     * <p>Shows specified players Name</p>
     * @param index specifies player id
     * @return Players name or null if index is out of bounds
     */
    public String getPlayersName(int index) {
        if (index >= players.size()) return null;
        return players.get(index).getName();
    }

    /**
     * <h2>The main function!</h2>
     * <p><b>This is Magical stuff!</b></p>
     * <p>Function for entering scores for each frame</p>
     * @param firstThrow first throw pins down from 0 to 10 or null if foul
     * @param secondThrow second throw pins down from 0 to 10 if needed or null if foul
     * @param additionalThrow additional throw pins down from 0 to 10 if needed and if last frame or null if foul
     * @return Score Table as a String
     */
    public String enterFrameScore(Integer firstThrow, Integer secondThrow, Integer additionalThrow) {
        if (!gameStarted) gameStarted = true;
        Integer index = getNextPlayer();
        if (index == null)
            return "";
        players.get(index).enterFrameScore(firstThrow, secondThrow, additionalThrow);
        return showResultTable();
    }

    public static void main(String[] args) {
        BowlingMachine bowlingMachine = new BowlingMachine();
        bowlingMachine.addPlayer("Taras");
        bowlingMachine.addPlayer("Artem");
        bowlingMachine.changePlayersName(1, "Sarat");

        bowlingMachine.enterFrameScore(3,5,0);
        bowlingMachine.enterFrameScore(3,7,0);

        bowlingMachine.enterFrameScore(10,0,0);
        bowlingMachine.enterFrameScore(5,5,0);

        bowlingMachine.enterFrameScore(4,5,0);
        bowlingMachine.enterFrameScore(7,2,0);

        bowlingMachine.enterFrameScore(null,5,0);
        bowlingMachine.enterFrameScore(7,null,0);

        bowlingMachine.enterFrameScore(10,0,0);
        bowlingMachine.enterFrameScore(10,0,0);

        bowlingMachine.enterFrameScore(10,0,0);
        bowlingMachine.enterFrameScore(10,0,0);

        bowlingMachine.enterFrameScore(4,5,0);
        bowlingMachine.enterFrameScore(7,2,0);

        bowlingMachine.enterFrameScore(10,0,0);
        bowlingMachine.enterFrameScore(10,0,0);

        bowlingMachine.enterFrameScore(10,0,0);
        bowlingMachine.enterFrameScore(1,9,0);

        bowlingMachine.enterFrameScore(3,4,0);
        bowlingMachine.enterFrameScore(9,1,null);

        System.out.println(bowlingMachine.showCurrentWinner());
        System.out.println(bowlingMachine.showResultTable());
    }
}
