package com.cerebrumtech.baklajan.bowling;

/**
 * User: andrey
 * Date: 3/1/14
 * Time: 2:52 PM
 */
public class SecondThrow extends AbstractThrow {
    private Integer pinsLeft;

    public SecondThrow(Integer pinsLeft) {
        this.pinsLeft = pinsLeft;
    }

    public int getPinsLeft() {
        return pinsLeft;
    }

    public void setPinsLeft(Integer pinsLeft) {
        this.pinsLeft = pinsLeft;
    }
}
