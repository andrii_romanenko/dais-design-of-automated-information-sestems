package com.cerebrumtech.baklajan.bowling;

import junit.framework.Assert;
import org.junit.Test;

/**
 * User: andrey
 * Date: 3/22/14
 * Time: 11:26 PM
 */
public class AcceptanceTest {
    @Test
    public void acceptanceTest() throws Exception {
        BowlingMachine bowlingMachine = new BowlingMachine();

        bowlingMachine.addPlayer("TestPlayer1");
        Assert.assertEquals((int)bowlingMachine.getNextPlayer(), 0);
        bowlingMachine.addPlayer("TestPlayer2");
        Assert.assertEquals((int)bowlingMachine.getNextPlayer(), 0);
        Assert.assertEquals(bowlingMachine.getPlayersName(0), "TestPlayer1");
        Assert.assertEquals(bowlingMachine.getPlayersName(1), "TestPlayer2");
        bowlingMachine.enterFrameScore(5,3,null);
        Assert.assertEquals((int)bowlingMachine.getNextPlayer(), 1);
        bowlingMachine.deletePlayer(1);
        Assert.assertEquals((int)bowlingMachine.getNextPlayer(), 0);
        Assert.assertEquals((int)bowlingMachine.getPlayersPoints(0), 8);

        bowlingMachine.enterFrameScore(10,0,0);
        bowlingMachine.enterFrameScore(10,0,0);
        bowlingMachine.enterFrameScore(1,1,0);

        Assert.assertEquals((int)bowlingMachine.getPlayersPoints(0), 43);

        bowlingMachine.enterFrameScore(5,5,0);
        bowlingMachine.enterFrameScore(10,0,0);
        bowlingMachine.enterFrameScore(5,3,0);

        Assert.assertEquals((int)bowlingMachine.getPlayersPoints(0), 89);

        bowlingMachine.enterFrameScore(null,5,0);
        bowlingMachine.enterFrameScore(6,null,0);
        bowlingMachine.enterFrameScore(10,10,10);

        String totalResult = "Name\t\t\t 1    2    3    4    5    6    7    8    9    10       Total\n" +
                        "TestPlayer1\t\t5 3  X -  X -  1 1  5 /  X -  5 3  F 5  6 F  X X X      160";

        Assert.assertEquals(bowlingMachine.showResultTable(), totalResult);
    }
}
