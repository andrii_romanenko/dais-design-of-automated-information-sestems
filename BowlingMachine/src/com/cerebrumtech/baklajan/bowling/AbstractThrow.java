package com.cerebrumtech.baklajan.bowling;

/**
 * User: andrey
 * Date: 3/1/14
 * Time: 2:50 PM
 */
public abstract class AbstractThrow {
    private boolean failed;
    private Integer pinsDown;

    public Integer getPinsDown() {
        return pinsDown;
    }

    public void setPinsDown(Integer pinsDown) {
        this.pinsDown = pinsDown;
    }

    public boolean isFailed() {

        return failed;
    }

    public void setFailed(boolean failed) {
        this.failed = failed;
    }
}
