package com.cerebrumtech.baklajan.bowling;

import junit.framework.Assert;
import org.junit.Test;

/**
 * User: andrey
 * Date: 3/18/14
 * Time: 12:31 AM
 */
public class FrameTest {
    @Test
    public void testStrikeEnterFrameScore() throws Exception {

        Frame frame = new Frame(false);

        frame.enterFrameScore(10, 0, 0);

        Assert.assertTrue(frame.isStrike());
        Assert.assertFalse(frame.isSpare());
        Assert.assertEquals((int)frame.getFirstThrowPInsDown(), 10);
        Assert.assertEquals((int)frame.getFramePinsDown(), 10);
        Assert.assertNull(frame.getAdditionalThrowPInsDown());
        Assert.assertNotNull(frame.getSecondThrowPInsDown());
    }

    @Test
    public void testSpareEnterFrameScore() throws Exception {

        Frame frame = new Frame(false);

        frame.enterFrameScore(1, 9, 0);

        Assert.assertFalse(frame.isStrike());
        Assert.assertTrue(frame.isSpare());
        Assert.assertEquals((int)frame.getFirstThrowPInsDown(), 1);
        Assert.assertEquals((int)frame.getSecondThrowPInsDown(), 9);
        Assert.assertEquals((int)frame.getFramePinsDown(), 10);
        Assert.assertNull(frame.getAdditionalThrowPInsDown());
        Assert.assertNotNull(frame.getSecondThrowPInsDown());
    }

    @Test
    public void testEnterLastFrameScore() throws Exception {

        Frame frame = new Frame(true);

        frame.enterFrameScore(10, 10, 10);

        Assert.assertTrue(frame.isStrike());
        Assert.assertFalse(frame.isSpare());
        Assert.assertEquals((int)frame.getFirstThrowPInsDown(), 10);
        Assert.assertEquals((int)frame.getSecondThrowPInsDown(), 10);
        Assert.assertEquals((int)frame.getAdditionalThrowPInsDown(), 10);
        Assert.assertEquals((int)frame.getFramePinsDown(), 30);
        Assert.assertNotNull(frame.getAdditionalThrowPInsDown());
        Assert.assertNotNull(frame.getSecondThrowPInsDown());
    }

}
