package com.cerebrumtech.baklajan.bowling;

/**
 * User: andrey
 * Date: 3/1/14
 * Time: 2:53 PM
 */
public class AdditionalThrow extends AbstractThrow {
    private Integer pinsLeft;

    public AdditionalThrow(Integer pinsLeft) {
        this.pinsLeft = pinsLeft;
    }

    public Integer getPinsLeft() {
        return pinsLeft;
    }

    public void setPinsLeft(Integer pinsLeft) {
        this.pinsLeft = pinsLeft;
    }
}
