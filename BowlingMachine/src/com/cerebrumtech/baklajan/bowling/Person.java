package com.cerebrumtech.baklajan.bowling;

import java.util.LinkedList;

/**
 * User: andrey
 * Date: 3/1/14
 * Time: 2:51 PM
 */
public class Person implements Player {
    private String name;
    LinkedList<Frame> frames = new LinkedList<Frame>();
    private Integer score;

    public Person(String name) {
        this.name = name;
        score = 0;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Integer getScore() {
        return score;
    }

    @Override
    public void enterFrameScore(Integer firstThrow, Integer secondThrow, Integer additionalThrow) {
        boolean isLastFrame = frames.size() == 9;
        Frame newFrame = new Frame(isLastFrame);
        newFrame.enterFrameScore(firstThrow, secondThrow, additionalThrow);

        updateScore(newFrame, isLastFrame);

        frames.add(newFrame);
    }

    private void updateScore(Frame newFrame, Boolean isLastFrame) {
        boolean allowPreviousCheck = frames.size() > 0;
        boolean allowDoubledPreviousCheck = frames.size() > 1;
        if (allowPreviousCheck && frames.getLast().isSpare()) {
            score += newFrame.getFirstThrowPInsDown();
        } else if (allowPreviousCheck && frames.getLast().isStrike()) {
            if (allowDoubledPreviousCheck && frames.get(frames.size() - 2).isStrike()) {
                score += newFrame.getFirstThrowPInsDown();
            }
            score += newFrame.getFirstThrowPInsDown() + newFrame.getSecondThrowPInsDown();
        }

        if (isLastFrame) {
            if (newFrame.getFirstThrowPInsDown() == 10) {
                score += newFrame.getSecondThrowPInsDown() + newFrame.getAdditionalThrowPInsDown();
                if (newFrame.getSecondThrowPInsDown() == 10) {
                    score += newFrame.getAdditionalThrowPInsDown();
                }
            } else if (newFrame.getFirstThrowPInsDown() + newFrame.getSecondThrowPInsDown() == 10) {
                score += newFrame.getAdditionalThrowPInsDown();
            }

        }
        score += newFrame.getFramePinsDown();

    }

    @Override
    public String showScoreTable() {
        String result = "";
        for (Frame frame : frames) {
            result += frame.getStringFramePinsDown() + "  ";
        }
        result += addResult();
        return result;
    }

    private String addResult() {
        String result = "";
        int framesLeft = 10 - frames.size();
        for (int i = 0; i < framesLeft; i++) {
            result += "     ";
        }
        result += (framesLeft == 0 && (frames.getLast().isStrike() || frames.getLast().isSpare())) ? "    " + score : "      " + score;
        return result;
    }


    @Override
    public Integer getFramesPlayed() {
        return frames.size();
    }
}
