package com.cerebrumtech.baklajan.bowling;

/**
 * User: andrey
 * Date: 3/1/14
 * Time: 2:49 PM
 */
public class Frame {
    private boolean lastFrame;
    private boolean strike;
    private boolean spare;
    private Integer framePinsDown;
    private FirstThrow firstThrow;
    private SecondThrow secondThrow;
    private AdditionalThrow additionalThrow;

    public Frame(Boolean lastFrame) {
        this.lastFrame = lastFrame;
    }

    public boolean isStrike() {
        return strike;
    }

    public boolean isSpare() {
        return spare;
    }

    public Integer getFramePinsDown() {
        return framePinsDown;
    }

    public Integer getFirstThrowPInsDown() {
        if (firstThrow == null) return null;
        return firstThrow.getPinsDown();
    }

    public Integer getSecondThrowPInsDown() {
        if (secondThrow == null) return null;
        return secondThrow.getPinsDown();
    }

    public Integer getAdditionalThrowPInsDown() {
        if (additionalThrow == null) return null;
        return additionalThrow.getPinsDown();
    }

    public String getStringFramePinsDown() {
        String result = "";
        result += strike ? "X " : firstThrow.isFailed() ? "F " : firstThrow.getPinsDown() + " ";
        if (strike) {
            if (!lastFrame) {
                result += "-";
            } else {
                result += secondThrow.getPinsDown() == 10 ? "X" : secondThrow.isFailed() ? "F" : secondThrow.getPinsDown();
                result += additionalThrow.getPinsDown() == 10 ? " X" : additionalThrow.isFailed() ? " F" : " " + additionalThrow.getPinsDown();
            }
        } else {
            result += spare ? "/" : secondThrow.isFailed() ? "F" : secondThrow.getPinsDown();
            if (spare && lastFrame) {
                result += additionalThrow.getPinsDown() == 10 ? " X " : additionalThrow.isFailed() ? " F" : " " + additionalThrow.getPinsDown();
            }
        }
        return result;
    }

    public void enterFrameScore(Integer firstPinsDown, Integer secondPinsDown, Integer additionalPinsDown) {
        boolean firstFailed = firstPinsDown == null;
        boolean secondFailed = secondPinsDown == null;
        boolean additionalFailed = additionalPinsDown == null;

        firstPinsDown = firstPinsDown == null ? 0 : firstPinsDown;
        secondPinsDown = secondPinsDown == null ? 0 : secondPinsDown;
        additionalPinsDown = additionalPinsDown == null ? 0 : additionalPinsDown;

        firstThrow = new FirstThrow(firstFailed, firstPinsDown);

        if (lastFrame) {
            secondThrow = new SecondThrow(10);
        } else {
            secondThrow = new SecondThrow(10 - firstPinsDown);
        }
        secondThrow.setFailed(secondFailed);
        secondThrow.setPinsDown(secondPinsDown);

        if (firstPinsDown == 10) {
            strike = true;
        }

        if (!strike && firstPinsDown + secondPinsDown == 10) {
            spare = true;
        }

        if (lastFrame && (strike || spare)) {
            if (secondPinsDown != 10) {
                additionalThrow = new AdditionalThrow(10 - secondPinsDown);
            } else {
                additionalThrow = new AdditionalThrow(10);
            }
            additionalThrow.setFailed(additionalFailed);
            additionalThrow.setPinsDown(additionalPinsDown);
        }

        framePinsDown = firstPinsDown + secondPinsDown + additionalPinsDown;
    }
}
